export interface ToDoUserEntry {
  Name: string;
  Done?: boolean;
  Due: Date | null;
}
