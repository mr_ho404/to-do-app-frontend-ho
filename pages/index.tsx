import type { GetServerSidePropsResult, NextPage } from "next";
import { useState } from "react";
import styled from "styled-components";
import { ToDoApiFactory } from "../api/ToDoApiFactory";
import { ToDoAddButton } from "../components/toDoAddButton";
import { ToDoAddDialog } from "../components/toDoAddDialog";
import { ToDoList } from "../components/toDoList";
import { ToDoUserEntry } from "../types";
import { ToDo } from "../types/ToDo";

const toDoApi = ToDoApiFactory.get();

interface HomepageProps {
  toDos: ToDo[];
}

export async function getServerSideProps(): Promise<
  GetServerSidePropsResult<HomepageProps>
> {
  return {
    props: {
      toDos: await toDoApi.getAllToDos(),
    },
  };
}

const Home: NextPage<HomepageProps> = ({
  toDos: toDosFromProps,
}: HomepageProps) => {
  const [toDos, setToDos] = useState<ToDo[]>(toDosFromProps);
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [showDone, setShowDone] = useState(false);

  const openToDoAddDialog = () => {
    setIsDialogOpen(true);
  };

  const closeToDoAddDialog = () => {
    setIsDialogOpen(false);
  };

  const filteredToDos = showDone ? toDos.filter((toDo) => !toDo.Done) : toDos;

  async function addToDoAddDialog(toDo: ToDoUserEntry): Promise<void> {
    await toDoApi.addToDo(toDo).then(async (response) => {
      closeToDoAddDialog();

      if (!response.ok) {
        Promise.resolve();
      }

      const newToDos = await toDoApi.getAllToDos();
      setToDos(() => [...newToDos]);
    });
  }

  async function updateToDoDoneStatus(
    id: number,
    done: boolean
  ): Promise<void> {
    const toDo: ToDo | undefined = toDos.find((toDo) => toDo.id === id);
    if (!toDo) {
      return;
    }

    const updatedToDo: ToDo = {
      ...toDo,
      Done: done,
    };

    setToDos((toDos) => [
      ...toDos.map((toDo) =>
        toDo.id === id && updatedToDo ? updatedToDo : toDo
      ),
    ]);
    await toDoApi.updateToDo(id, updatedToDo);
  }

  function toggleFilterButton() {
    setShowDone((showDone) => !showDone);
  }

  const ToggleButton = styled.button`
    border-radius: 12px;
    background-color: #eee;
    font-size: 16px;
    width: 100%;
    padding: 12px;
    margin-bottom: 16px;
    box-shadow: #ccc 2px 2px 3px;
  `;

  return (
    <>
      <h1>To-Dos</h1>
      <ToDoAddButton onClick={openToDoAddDialog}>Add To-Do</ToDoAddButton>
      <ToggleButton onClick={toggleFilterButton}>
        {showDone ? "Show All To-Dos" : "Hide Done To-Dos"}
      </ToggleButton>
      <hr />
      <ToDoList
        toDos={filteredToDos}
        updateToDoDoneStatus={updateToDoDoneStatus}
      />

      {/* <---Dialogs---> */}
      <ToDoAddDialog
        open={isDialogOpen}
        onClose={closeToDoAddDialog}
        onAdd={addToDoAddDialog}
      />
    </>
  );
};

export default Home;
