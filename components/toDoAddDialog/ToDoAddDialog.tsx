import { useState } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { ToDoUserEntry } from "../../types";
import {
  Button,
  ButtonsWrapper,
  DatePickerWrapper,
  DialogBackground,
  DialogBox,
  Input,
  InputLabel,
  InputWrapper,
  RadioElement,
  RadioInput,
  Title,
} from "./ToDoAddDialog.styled";

interface ToDoAddDialogProps {
  open: boolean;
  onClose: () => void;
  onAdd: (toDo: ToDoUserEntry) => void;
}

export function ToDoAddDialog({
  open,
  onClose,
  onAdd,
}: ToDoAddDialogProps): JSX.Element | null {
  const [toDoName, setToDoName] = useState("");
  const [toDoDueDate, setToDoDueDate] = useState<Date | null>(null);
  const [toDoStatus, setToDoStatus] = useState(false);

  const handleAdd = () => {
    const entry: ToDoUserEntry = {
      Name: toDoName,
      Due: toDoDueDate,
      Done: toDoStatus,
    };
    onAdd(entry);

    setToDoName("");
    setToDoDueDate(null);
    setToDoStatus(false);
  };

  return !open ? null : (
    <DialogBackground>
      <DialogBox>
        <Title>Add To-Do</Title>

        <InputWrapper>
          <InputLabel htmlFor="toDoName">Name:</InputLabel>
          <Input
            id="toDoName"
            type="text"
            value={toDoName}
            onChange={(evt) => setToDoName(evt.target.value)}
          />

          <DatePickerWrapper>
            <InputLabel htmlFor="toDoDueDate">Due Date:</InputLabel>
            <DatePicker
              id="toDoDueDate"
              selected={toDoDueDate}
              onChange={(date: Date) => setToDoDueDate(date)}
            />
          </DatePickerWrapper>

          <InputLabel htmlFor="toDoStatus">Status:</InputLabel>
          <RadioElement>
            <RadioInput
              id="toDoStatus"
              type="radio"
              name="status"
              value="done"
              checked={toDoStatus}
              onChange={(_evt) => setToDoStatus(true)}
            />
            <label htmlFor="toDoStatus">Done</label>
          </RadioElement>
          <RadioElement>
            <RadioInput
              id="toDoStatus"
              type="radio"
              name="status"
              value="not-done"
              checked={!toDoStatus}
              onChange={(_evt) => setToDoStatus(false)}
            />
            <label htmlFor="toDoStatus">Not Done</label>
          </RadioElement>
        </InputWrapper>

        <ButtonsWrapper>
          <Button onClick={onClose}>Cancel</Button>
          <Button onClick={handleAdd} disabled={!toDoName}>
            Add
          </Button>
        </ButtonsWrapper>
      </DialogBox>
    </DialogBackground>
  );
}
