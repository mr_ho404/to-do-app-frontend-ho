import styled from "styled-components";

export const DialogBackground = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.5);
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const DialogBox = styled.div`
  background: white;
  padding: 16px;
  border-radius: 4px;
  width: 50%;
`;

export const Title = styled.h2`
  margin-top: 0;
`;

export const InputWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 32px;
`;

export const InputLabel = styled.label`
  font-size: 16px;
  font-weight: semibold;
  margin-right: 16px;
`;

export const Input = styled.input`
  margin-bottom: 8px;
  width 50%
`;

export const RadioElement = styled.div`
  flex-direction: row;
`;

export const RadioInput = styled.input`
  margin-right: 8px;
`;

export const ButtonsWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 16px;
`;

export const Button = styled.button`
  margin-left: 8px;
`;

export const DatePickerLabel = styled.label`
  font-size: 16px;
  font-weight: semibold;
  margin-right: 8px;
`;

export const DatePickerWrapper = styled.div`
  .react-datepicker-wrapper {
    display: flex;
    flex-direction: column;
    margin-bottom: 8px;

    input {
      width 50%
    }
  }
`;
