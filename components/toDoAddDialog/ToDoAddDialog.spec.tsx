import { fireEvent, render, RenderResult } from "@testing-library/react";
import { ToDoUserEntry } from "../../types";
import { ToDoAddDialog } from "./ToDoAddDialog";

jest.mock("react-datepicker/dist/react-datepicker.css", () => {
  return {};
});

describe("ToDoAddDialog", () => {
  let renderResult: RenderResult;
  let toDoEntry: ToDoUserEntry;
  let mockCloseFn: () => void;
  let mockAddFn: (toDo: ToDoUserEntry) => void;

  beforeEach(() => {
    toDoEntry = {
      Name: "To do 1",
      Done: false,
      Due: new Date("01/01/1970"),
    };
    mockCloseFn = jest.fn();
    mockAddFn = jest.fn();
  });

  it("renders the dialog when open is true", () => {
    renderResult = render(
      <ToDoAddDialog open={true} onClose={mockCloseFn} onAdd={mockAddFn} />
    );

    expect(renderResult.getByText("Add To-Do")).toBeInTheDocument();
    expect(
      renderResult.container.querySelector("input[type=text]")
    ).toBeTruthy();
  });

  it("does not render the dialog when open is false", () => {
    renderResult = render(
      <ToDoAddDialog open={false} onClose={mockCloseFn} onAdd={mockAddFn} />
    );
    expect(renderResult.queryByText("Add To-Do")).not.toBeInTheDocument();
  });

  it("calls onClose when Cancel button is clicked", () => {
    const onClose = jest.fn();
    renderResult = render(
      <ToDoAddDialog open={true} onClose={onClose} onAdd={mockAddFn} />
    );
    fireEvent.click(renderResult.getByText("Cancel"));
    expect(onClose).toHaveBeenCalledTimes(1);
  });

  it("calls onAdd with correct arguments when Add button is clicked", () => {
    const onAdd = jest.fn();
    renderResult = render(
      <ToDoAddDialog open={true} onClose={mockCloseFn} onAdd={onAdd} />
    );

    fireEvent.change(renderResult.getByLabelText("Name:"), {
      target: { value: "Test ToDo" },
    });
    fireEvent.change(renderResult.getByLabelText("Due Date:"), {
      target: { value: "01/01/1970" },
    });
    fireEvent.click(renderResult.getByLabelText("Done"));

    fireEvent.click(renderResult.getByText("Add"));

    expect(onAdd).toHaveBeenCalledWith({
      Name: "Test ToDo",
      Due: new Date("01/01/1970"),
      Done: true,
    });
  });
});
