import { render, RenderResult } from "@testing-library/react";
import { mockToDos } from "../../lib-test/mock-todos";
import { ToDo } from "../../types/ToDo";
import { ToDoList } from "./ToDoList";

describe("ToDoList", () => {
  let renderResult: RenderResult;
  let updateToDoDoneStatusMock = jest.fn();

  it("should render all to-dos", () => {
    renderResult = render(
      <ToDoList
        toDos={mockToDos}
        updateToDoDoneStatus={updateToDoDoneStatusMock}
      />
    );

    expect(renderResult.container).toBeTruthy();
    expect(renderResult.findAllByRole("heading")).not.toBeNull;
    expect(renderResult.queryAllByTestId("to-do-entry")).toHaveLength(2);

    for (const toDo of mockToDos) {
      expect(renderResult.getByText(toDo.Name ?? "")).toBeInTheDocument();
    }
  });

  it("should deal with non iterable toDos array gracefully", () => {
    render(
      <ToDoList
        toDos={null as unknown as ToDo[]}
        updateToDoDoneStatus={updateToDoDoneStatusMock}
      />
    );

    expect(renderResult.findAllByRole("heading")).not.toBeNull;
    expect(renderResult.queryAllByTestId("to-do-entry")).toHaveLength(0);
  });
});
