import { ToDo } from "../../types";
import { ToDoEntry } from "../toDoEntry";

export interface ToDoListProps {
  toDos: ToDo[];
  updateToDoDoneStatus: (id: number, done: boolean) => void;
}

export function ToDoList({
  toDos,
  updateToDoDoneStatus,
}: ToDoListProps): JSX.Element {
  // toDos isn't an iterable, causing an error... Not sure why
  const sortedToDos = Array.from(toDos || []).sort(
    (t1, t2) => +(t1.Done || false) - +(t2.Done || false)
  );

  return (
    <>
      {sortedToDos.map((toDo) => (
        <ToDoEntry
          key={toDo.id}
          toDo={toDo}
          updateToDoDoneStatus={updateToDoDoneStatus}
        />
      ))}
    </>
  );
}
