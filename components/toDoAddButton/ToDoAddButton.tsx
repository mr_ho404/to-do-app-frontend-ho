import styled from "styled-components";

const AddButton = styled.button`
  border-radius: 12px;
  background-color: green;
  font-size: 24px;
  width: 100%;
  padding: 24px;
  color: white;
  margin-bottom: 16px;
  box-shadow: #ccc 2px 2px 3px;
  outline: none;
`;

type ButtonProps = {
  onClick: () => void;
  children: React.ReactNode;
};

export function ToDoAddButton({ onClick, children }: ButtonProps): JSX.Element {
  return <AddButton onClick={onClick}>{children}</AddButton>;
}
