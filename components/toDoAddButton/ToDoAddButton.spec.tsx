import { render, RenderResult, screen } from "@testing-library/react";
import { ToDoAddButton } from "./ToDoAddButton";

describe("Button", () => {
  let renderResult: RenderResult;
  let button;
  const handleClick = jest.fn();

  beforeEach(() => {
    renderResult = render(
      <ToDoAddButton onClick={handleClick}>Add To-Do</ToDoAddButton>
    );
  });

  it("renders correctly", () => {
    button = screen.getByRole("button");

    expect(button).toBeInTheDocument();
    expect(button).toHaveTextContent("Add To-Do");
  });

  it("calls onClick handler when clicked", () => {
    button = screen.getByRole("button");
    button.click();

    expect(handleClick).toHaveBeenCalledTimes(1);
  });
});
