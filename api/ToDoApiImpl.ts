import { ToDo, ToDoUserEntry } from "../types";
import { ToDoApi } from "./ToDoApi";

export class ToDoApiImpl implements ToDoApi {
  constructor(
    private BACKEND_URL = process.env.NEXT_PUBLIC_BACKEND_URL ||
      "http://localhost:1337",
    private TO_DO_ENDPOINT = "/to-dos"
  ) {}

  getAllToDos(): Promise<ToDo[]> {
    return fetch(`${this.BACKEND_URL}${this.TO_DO_ENDPOINT}`).then((result) =>
      result.json()
    );
  }

  updateToDo(id: number, toDo: ToDo): Promise<void> {
    return fetch(`${this.BACKEND_URL}${this.TO_DO_ENDPOINT}/${id}`, {
      method: "PUT",
      body: JSON.stringify(toDo),
      headers: {
        "Content-Type": "application/json",
      },
    }).then(() => {});
  }

  addToDo(toDoUserEntry: ToDoUserEntry): Promise<Response> {
    const url = `${this.BACKEND_URL}${this.TO_DO_ENDPOINT}`;

    return fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(toDoUserEntry),
    });
  }
}
