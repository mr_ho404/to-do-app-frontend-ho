import { mockToDos } from "../lib-test/mock-todos";
import { ToDoUserEntry } from "../types";
import { ToDoApi } from "./ToDoApi";
import { ToDoApiImpl } from "./ToDoApiImpl";

describe("To Do API", () => {
  let toDoApi: ToDoApi;
  let backendUrl = "http://backend";
  let toDoEndpoint = "/to-dos";
  let fetchMock: jest.Mock;

  beforeEach(() => (toDoApi = new ToDoApiImpl(backendUrl, toDoEndpoint)));

  afterEach(() => fetchMock.mockClear());

  it("should fetch all to-dos from an HTTP endpoint", async () => {
    // Setup
    fetchMock = jest.fn().mockReturnValue(
      Promise.resolve({
        json: () => Promise.resolve(mockToDos),
      }) as Promise<Response>
    );
    global.fetch = fetchMock;

    // Test
    expect(await toDoApi.getAllToDos()).toEqual(mockToDos);
    expect(fetchMock).toHaveBeenCalledWith(`${backendUrl}${toDoEndpoint}`);
  });

  it("should update a to-do using the HTTP endpoint", async () => {
    // Setup
    fetchMock = jest
      .fn()
      .mockReturnValue(Promise.resolve({ json: () => Promise.resolve() }));
    global.fetch = fetchMock;

    const updatedToDo = { id: 0, Name: "new name", Done: false };

    // Test
    await toDoApi.updateToDo(updatedToDo.id, updatedToDo);

    expect(fetchMock).toHaveBeenCalledWith(
      `${backendUrl}${toDoEndpoint}/${updatedToDo.id}`,
      {
        body: JSON.stringify(updatedToDo),
        headers: {
          "Content-Type": "application/json",
        },
        method: "PUT",
      }
    );
  });

  it("should create a new to-do using the HTTP endpoint", async () => {
    // Setup
    fetchMock = jest
      .fn()
      .mockReturnValue(Promise.resolve({}) as Promise<Response>);
    global.fetch = fetchMock;

    const newToDo: ToDoUserEntry = {
      Name: "Netflix and Chill",
      Due: new Date("01/01/1970"),
      Done: false,
    };

    // Test
    await toDoApi.addToDo(newToDo);

    expect(fetchMock).toHaveBeenCalledWith(`${backendUrl}${toDoEndpoint}`, {
      body: JSON.stringify(newToDo),
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
    });
  });
});
