import { ToDoUserEntry } from "../types";
import { ToDo } from "../types/ToDo";

export interface ToDoApi {
  getAllToDos(): Promise<ToDo[]>;
  updateToDo(id: number, toDo: ToDo): Promise<void>; //ToDo has id, why pass ID separately?
  addToDo(toDoUserEntry: ToDoUserEntry): Promise<Response>;
}
